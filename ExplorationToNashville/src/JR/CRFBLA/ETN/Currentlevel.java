package jr.crfbla.etn;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Currentlevel extends Activity {
	/** Called when the activity is first created/ */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.currentlevel);
	}
	
	
	// Do something in response to button
	  public void categoryEasy (View view) 
	  { 
		Intent easy = new Intent(Currentlevel.this, Easy.class);
			Currentlevel.this.startActivity(easy);
		    
		    } 
		


  public void categoryMedium (View view) 
  { 
	Intent medium = new Intent(Currentlevel.this, Medium.class);
		Currentlevel.this.startActivity(medium);
	    
	    } 
	


public void categoryHard (View view) 
{ 
	Intent hard = new Intent(Currentlevel.this, Hard.class);
		Currentlevel.this.startActivity(hard);
	    
	    } 
	
}